﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionBase : MonoBehaviour
{
    public static UnityAction<ActionBase, Characters_Actions> CompleteAction;

    public Characters character;

    public virtual void BeginAction(Characters_Actions action)
    {

    }

    protected virtual void EndAction(Characters_Actions action)
    {
        if (CompleteAction != null)
        {
            Debug.Log("End Action " + action + ": " + character);
            CompleteAction(this, action);
        }
    }
}

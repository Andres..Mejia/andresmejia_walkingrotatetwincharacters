﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeInfo : MonoBehaviour
{
    [Header("Lover1")]
    [SerializeField]
    private Characters_Actions action1 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action2 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action3 = Characters_Actions.No_Action;

    [Header("Lover2")]
    [SerializeField]
    private Characters_Actions action4 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action5 = Characters_Actions.No_Action;
    [SerializeField]
    private Characters_Actions action6 = Characters_Actions.No_Action;

    public CubeActions GetActionsLover1()
    {

        CubeActions actions = new CubeActions();
        actions.action1 = action1;
        actions.action2 = action2;
        actions.action3 = action3;
        return actions;
    }

    public CubeActions GetActionsLover2()
    {

        CubeActions actions = new CubeActions();
        actions.action1 = action4;
        actions.action2 = action5;
        actions.action3 = action6;
        return actions;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Rotation : ActionBase
{
    private enum RotationStates { InitMove, ApplyRotation, FinalMove }
    private float delayToRotate = 15;
    private Characters_Actions actionTmp;

    public override void BeginAction(Characters_Actions action)
    {
        Debug.Log(character + " BeginAction: " + action);
        base.BeginAction(action);

        actionTmp = action;

        switch (action)
        {
            case Characters_Actions.Turn_Left:
                StartCoroutine(_Rotation(Vector3.down, 90));

                break;
            case Characters_Actions.Turn_Right:
                StartCoroutine(_Rotation(Vector3.up, 90));

                break;
            case Characters_Actions.Turn_Forward:
                RotationManager(RotationStates.InitMove);
                break;

            case Characters_Actions.Turn_Backward:
                RotationManagerBack(RotationStates.InitMove);
                break;
            default:
                break;
        }

    }


    private IEnumerator _Rotation(Vector3 referenceAxis, float finalAngle)
    {
        float valueToRotate = 0;
        while (valueToRotate < finalAngle)
        {
            transform.Rotate(referenceAxis, delayToRotate, Space.Self);
            valueToRotate += delayToRotate;
            yield return new WaitForSeconds(0.1f);
        }
        //TODO: Este codigo debe mejorar
        if (actionTmp != Characters_Actions.Turn_Forward)
        {
            EndAction(actionTmp);
        }
        else
        {
            RotationManager(RotationStates.FinalMove);
        }
    }

    private IEnumerator MoveForRotate(RotationStates state)
    {
        float move = 0;
        while (move < 0.5f)
        {
            transform.Translate(transform.forward * 0.1f, Space.World);
            move = move + 0.1f;
            yield return new WaitForSeconds(0.1f);
        }

        //TODO: Este codigo debe mejorar
        if (state == RotationStates.InitMove)
        {
            RotationManager(RotationStates.ApplyRotation);
        }
        else
        {
            EndAction(actionTmp);
        }

    }

    private void RotationManager(RotationStates state)
    {
        switch (state)
        {
            case RotationStates.InitMove:
                StartCoroutine(MoveForRotate(state));

                break;
            case RotationStates.ApplyRotation:
                StartCoroutine(_Rotation(Vector3.right, 90));

                break;
            case RotationStates.FinalMove:
                StartCoroutine(MoveForRotate(state));
                break;
        }
    }

    private void RotationManagerBack(RotationStates state)
    {
        switch (state)
        {
            case RotationStates.InitMove:
                StartCoroutine(MoveForRotate(state));

                break;
            case RotationStates.ApplyRotation:
                StartCoroutine(_Rotation(Vector3.right, -90));

                break;
            case RotationStates.FinalMove:
                StartCoroutine(MoveForRotate(state));
                break;
        }
    }
}
